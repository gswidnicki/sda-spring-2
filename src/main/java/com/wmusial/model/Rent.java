package com.wmusial.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "rent")
public class Rent extends BaseEntity {

    @Column(name = "first_name")
    private Date createdDate;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book book;

    public Rent() {
    }

    public Rent(Date createdDate, User user, Book book) {
        this.createdDate = createdDate;
        this.user = user;
        this.book = book;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
